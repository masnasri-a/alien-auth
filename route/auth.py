from bson import ObjectId
from fastapi import APIRouter
from fastapi.exceptions import HTTPException
from model.auth.register import model
from config.mongo import get_mongo_client
from util.service import validate_client_token
from util.token import otp_generator
from util.smtp import send_email

router = APIRouter()

@router.post("/register")
async def register(body: model):
    if not validate_client_token(body.client_token):
            raise HTTPException(status_code=401, detail="Unauthorized")
    try:
        client, coll = get_mongo_client('users')
        with client:
            data = coll.find_one({"email":body.email})
            if data:
                return {"message": "Email already exists"}
            else:
                otp = otp_generator()
                coll.insert_one({
                    "_id": str(ObjectId()),
                    "client_token": body.client_token,
                    "username": body.username,
                    "email": body.email,
                    "password": body.password,
                    "role": body.role,
                    "otp": otp,
                    "activate": False
                })
                send_email(receiver_email=body.email, name=body.username, otp=otp)
                return {"message": "User created successfully, Please validate your email address to activate your account."}
    except Exception as e:
        raise HTTPException(status_code=500, detail=str(e))
    
@router.post("/revalidate_email")
async def revalidate_email(email: str, client_token: str):
    try:
        client, coll = get_mongo_client('users')
        with client:
            filter = {"email": email, "client_token": client_token}
            data = coll.find_one(filter=filter)
            if data:
                otp = otp_generator()
                coll.update_one(filter, {"$set": {"otp": otp, "activate": False}})
                send_email(receiver_email=email, name=data["username"], otp=otp)
                return {"message": "Email sent successfully"}
            else:
                return HTTPException(status_code=404, detail="Email not found")
    except HTTPException as e:
        raise e
    except Exception as e:
        raise HTTPException(status_code=500, detail=str(e))
    
@router.post("/activate_user")
async def activate_user(email: str, otp: str, client_token: str):
    try:
        client, coll = get_mongo_client('users')
        with client:
            filter = {"email": email, "client_token": client_token, "otp": otp}
            data = coll.find_one(filter=filter)
            if not data:
                raise HTTPException(status_code=404, detail="User not found")
            if data["activate"] == True:
                raise HTTPException(status_code=400, detail="User already activated")
            coll.update_one(filter, {"$set": {"activate": True}})
            return {"message": "User activated successfully"}
    except HTTPException as e:
        raise e
    except Exception as e:
        raise HTTPException(status_code=500, detail=str(e))
            
@router.post("/login")
async def login(email: str, password: str, client_token: str):
    try:
        client, coll = get_mongo_client('users')
        with client:
            filter = {"email": email, "client_token": client_token, "password": password, "activate": True}
            data = coll.find_one(filter=filter)
            if data:
                return {"message": "Login success"}
            else:
                raise HTTPException(status_code=401, detail="Invalid email or password")
    except HTTPException as e:
        raise e
    except Exception as e:
        raise HTTPException(status_code=500, detail=str(e))