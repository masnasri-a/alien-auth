

from pydantic import BaseModel


class model(BaseModel):
    client_name: str
    client_email: str
    callback_url: str
    role: str
    