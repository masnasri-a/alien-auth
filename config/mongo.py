from pymongo import MongoClient
from dotenv import load_dotenv
import os

def get_mongo_client(collection:str = "users"):
    try:
        load_dotenv()
        mongo_uri = "mongodb://nasri:UtyCantik12@103.139.192.26:27017/"
        client = MongoClient(mongo_uri)
        print(client)
        db = client["AlienAuth"]
        coll = db[collection]
        return client, coll
    except Exception as e:
        print(e)
        return {"error": str(e)}