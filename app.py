from fastapi import FastAPI
from fastapi.responses import RedirectResponse
from route.mgmt import router as mgmt_router
from route.auth import router as auth_router

app = FastAPI(
    title="AlienAuth Service",
    description="This is a simple service to authenticate users",
    version="0.1",
    contact={
        "name": "Nasri Adzlani",
        "url": "https://alien.com",
        "email": "nasriblog12@gmail.com",
    },
    license_info={
        "name": "MIT",
        "url": "https://opensource.org/licenses/MIT",
    }
)

@app.get("/")
async def root():
    return RedirectResponse(url="/docs")

app.include_router(mgmt_router, prefix="/mgmt", tags=["Management"])
app.include_router(auth_router, prefix="/auth", tags=["Authentication"])

if __name__ == "__main__":
    import uvicorn
    uvicorn.run(app, host="0.0.0.0", port=8000)

