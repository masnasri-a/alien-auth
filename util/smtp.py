
from email.mime.image import MIMEImage
import smtplib
from email.mime.text import MIMEText
from email.mime.multipart import MIMEMultipart
import ssl
import os
from dotenv import load_dotenv
from jinja2 import Environment, FileSystemLoader


def send_email(receiver_email, name, otp):
    env = Environment(loader=FileSystemLoader('.'))
    template = env.get_template('template/otp.html')
    html_content = template.render(name=name, otp=otp)
    load_dotenv()
    sender_email = os.getenv("SMTP_EMAIL")
    smtp_server = os.getenv("SMTP_HOST")
    smtp_port = os.getenv("SMTP_PORT")
    smtp_password = os.getenv("SMTP_PASSWORD")
    print(f"Sending email to {receiver_email}...")
    # Create a multipart message
    msg = MIMEMultipart()
    msg["From"] = sender_email
    msg["To"] = receiver_email
    msg["Subject"] = "AlienAuth OTP Verification"
    msg.attach(MIMEText(html_content, "html"))
    image_path = "template/logo.png"
    with open(image_path, "rb") as image_file:
        image_data = image_file.read()
        image_part = MIMEImage(image_data, name=os.path.basename(image_path))
        image_part.add_header('Content-ID', '<logo>')
        msg.attach(image_part)
    image_auth = "template/auth.png"
    with open(image_auth, "rb") as image_file:
        image_data = image_file.read()
        image_part = MIMEImage(image_data, name=os.path.basename(image_auth))
        image_part.add_header('Content-ID', '<activation-code>')
        msg.attach(image_part)

    text = msg.as_string()

    context = ssl.create_default_context()

    try:
        with smtplib.SMTP_SSL(smtp_server, int(smtp_port), context=context) as server:
            server.login(sender_email, smtp_password)
            server.ehlo()
            server.sendmail(sender_email, receiver_email, text)
        print("Email sent successfully!")
    except Exception as e:
        print(f"An error occurred while sending the email: {str(e)}")
