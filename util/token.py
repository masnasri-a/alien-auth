import random
import string

def generate_nanoid(length):
    characters = string.ascii_lowercase + string.ascii_uppercase + string.digits
    nanoid = ''.join(random.choice(characters) for _ in range(length))
    return nanoid

def otp_generator():
    otp = ''.join(random.choice(string.digits) for _ in range(6))
    return otp