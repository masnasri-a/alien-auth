from config.mongo import get_mongo_client

def validate_client_token(client_token: str):
    client, coll = get_mongo_client('workspace')
    with client:
        filter={
            'client_token': client_token, 
            'activate': True
        }
        data = coll.find_one(filter=filter)
        if data:
            return True
        else:
            return False
    
