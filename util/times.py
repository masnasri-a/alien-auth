from time import time


def created_at_timestamp():
    return int(time() * 1000)

def expired_by_role(role):
    if role == "trial":
        return created_at_timestamp() + 2592000000
    elif role == "premium_3m":
        return created_at_timestamp() + 7776000000
    elif role == "premium_6m":
        return created_at_timestamp() + 15552000000
    elif role == "premium_1y":
        return created_at_timestamp() + 31536000000
    else:
        return 0