from bson import ObjectId
from fastapi import APIRouter
from config.mongo import get_mongo_client
from model.mgmt.create import model
from nanoid import generate
from util.times import expired_by_role, created_at_timestamp
from util.token import otp_generator
from util.smtp import send_email

router = APIRouter()

@router.post("/create_client")
async def create_client(body: model):
    try:
        client, coll = get_mongo_client('workspace')
        with client:
            otp = otp_generator()
            coll.insert_one({
                "_id": str(ObjectId()),
                "client_email": body.client_email,
                "client_token": generate(),
                "client_name": body.client_name,
                "callback_url": body.callback_url,
                "role": body.role,
                "expire_at": expired_by_role(body.role),
                "created_at": created_at_timestamp(),
                "updated_at": created_at_timestamp(),
                "otp": otp,
                "activate": False
            })
            send_email(receiver_email=body.client_email, name=body.client_name, otp=otp)
            return {"message": "Client created successfully"}
    except Exception as e:
        return {"error": str(e)}
    
@router.post("/activate_client")
async def activate_client(email: str, otp: str):
    client, coll = get_mongo_client('workspace')
    with client:
        data = coll.find_one({"client_email":email})
        if data["otp"] == otp:
            coll.update_one({"client_email":email}, {"$set": {"activate": True}})
            return {"message": "Client activated successfully"}
        else:
            return {"message": "Invalid OTP"}
        
@router.get("/get_otp")
async def get_client(email: str):
    client, coll = get_mongo_client('workspace')
    with client:
        data = coll.find_one({"client_email":email})
        return {"otp": data["otp"]}


