from pydantic import BaseModel

class model(BaseModel):
    client_token: str
    username: str
    email: str
    password: str
    role: str